import os

import gitlab
import flowdock

# API tokens
FLOWDOCK_API_TOKEN = os.environ['FLOWDOCK_API_TOKEN']
GITLAB_API_TOKEN = os.environ['GITLAB_API_TOKEN']
GITLAB_GROUP = os.environ['GITLAB_GROUP']
FLOW_ID = os.environ['FLOW_ID']
FLOW_ORG = os.environ['FLOW_ORG']

# Get the open Satoshi Merge requests.
def main():
    message = "@team\n"
    gl = gitlab.Gitlab('https://www.gitlab.com', private_token=GITLAB_API_TOKEN)
    group = gl.groups.get(GITLAB_GROUP)
    mrs = group.mergerequests.list(state="opened")
    non_wips = (mr for mr in mrs if not mr.work_in_progress)
    for mr in non_wips:
        message += "{} - {} - {}\n".format(mr.references['full'], mr.title , mr.web_url)

    print(message)

    client = flowdock.connect(token=FLOWDOCK_API_TOKEN)
    flow = client(org=FLOW_ORG, flow=FLOW_ID)
    flow.send(message)

main()
