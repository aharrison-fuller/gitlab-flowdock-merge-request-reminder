FROM python:alpine

RUN mkdir /app
COPY ./main.py /app
COPY ./requirements.pip /app
RUN pip install -r /app/requirements.pip

ENTRYPOINT python /app/main.py